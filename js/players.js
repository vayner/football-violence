var friendlyTeam;				//contains all friendly players
var opposingTeam;				//contains all opposing players

function Player(nr, sname, color, x, y, iteam) {
	this.number = nr;
	this.surname = sname;
	this.health = 100;
	this.fear = 0;
	this.tiredness = 0;
	this.hasBall = false;
	this.team = iteam;
	this.originalY = y;
	this.topSpeed = Math.ceil(Math.random() * 5);

	this.circle = new createjs.Shape();
 	
 	this.circle.graphics.beginFill(color).drawCircle(0, 0, 10);
 	this.circle.x=x;
 	this.circle.y=y;

 	fieldContainer.addChild(this.circle);
}

function Team(right) {
	this.offensiveLine = 0;
	this.offensiveTop = 0;
	this.offensiveBottom = 0;
	this.midLine = 0;
	this.midTop = 0;
	this.modBottom = 0;
	this.defensiveLine = 0;
	this.defensiveTop = 0;
	this.defensiveBottom = 0;

	this.hasBall = false;
	this.right=right;
	this.players = [];
	this.keeper;
	this.offensive = [];
	this.mid = [];
	this.defensive = [];
}

function GenerateTeam(color, right, defensive, mid, offensive) {
	iteam = new Team(right);
	if (right) {
		iteam.keeper = new Player(1, "Mr. Keeper", color, 715, 238, iteam);
	}
	else {
		iteam.keeper = new Player(1, "Mr. Keeper", color, 20, 238, iteam);
	}

	iteam.players.push(iteam.keeper);

	for (var i = 1; i <= defensive; i++) {
		x = 0;
		y = (476/(defensive + 1)) * i;
		if (right) {
			x = 735 - x;
		};

		var def = new Player(i+3, "Defender " + i, color, x, y, iteam);
		iteam.defensive.push(def);
		iteam.players.push(def);
	};

	for (var i = 1; i <= mid; i++) {
		x = 0;
		y = (476/(mid + 1)) * i;
		if (right) {
			x = 735 - x;
		};

		var midp = new Player(i+18, "Mid " + i, color, x, y, iteam);
		iteam.mid.push(midp);
		iteam.players.push(midp);
	};

	for (var i = 1; i <= offensive; i++) {
		x = 0;
		y = (476/(offensive + 1)) * i;
		if (right) {
			x = 735 - x;
		};

		var off = new Player(i+30, "Offensive " + i, color, x, y, iteam); 
		iteam.offensive.push(off);
		iteam.players.push(off);
	};

	return iteam;
}

function UpdatePlayers() {
	MoveTeam(friendlyTeam, false);
	MoveTeam(opposingTeam, true);
}

function MoveTeam(iteam, right) {
	var x = (right) ? field.image.width - ball.x : ball.x;
	
	if (iteam.hasBall) {
		iteam.offensiveLine = x;
	}
	else {
		iteam.offensiveLine = x - 20;
	}

	iteam.midLine = (iteam.offensiveLine / 3) * 2;
	iteam.defensiveLine = iteam.midLine / 2;

	//set the ideal horsontal lines.

	iteam.offensiveTop = ball.y - (((x - iteam.offensiveLine) / x) * ball.y);
	iteam.offensiveBottom = (((x - iteam.offensiveLine) / x) * (field.image.height - ball.y)) + ball.y;

	iteam.midTop = ball.y - (((x - iteam.midLine) / x) * ball.y);
	iteam.midBottom = (((x - iteam.midLine) / x) * (field.image.height - ball.y)) + ball.y;

	iteam.defensiveTop = ball.y - (((x - iteam.defensiveLine) / x) * ball.y);
	iteam.defensiveBottom = (((x - iteam.defensiveLine) / x) * (field.image.height - ball.y)) + ball.y;


	if (right) {
		iteam.offensiveLine = 735 - iteam.offensiveLine;
		iteam.defensiveLine = 735 - iteam.defensiveLine;
		iteam.midLine = 735 - iteam.midLine;
	};




	for (var i = iteam.defensive.length - 1; i >= 0; i--) {
		MoveDefender(iteam.defensive[i], i, right);
	};

	for (var i = iteam.mid.length - 1; i >= 0; i--) {
		MoveMid(iteam.mid[i], i, right);
	};

	for (var i = iteam.offensive.length - 1; i >= 0; i--) {
		MoveOffensive(iteam.offensive[i], i);
	};

	if (iteam.keeper != null) {
		MoveKeeper(iteam.keeper);
	};

	
}

function MoveDefender(iplayer, nr) {
	var idealX = iplayer.team.defensiveLine;
	var idealY;
	if (iplayer.hasBall) {
		idealX = (iplayer.team.right) ? 0 : field.image.width;
		idealY = field.image.height / 2;
	}
	else {
		if (iplayer.team.defensive.lenght > 1) {
			idealY = iplayer.team.defensiveTop + (((iplayer.team.defensiveBottom - iplayer.team.defensiveTop) / (iplayer.team.defensive.length - 1)) * nr - 1); 
		}
		else {
			idealY = iplayer.team.defensiveTop + ((iplayer.team.defensiveBottom - iplayer.team.defensiveTop) / 2);
		}
	}

	//Never make a robot that does not fear humans.
	if (iplayer.team == opposingTeam && Math.hypot(PC.c.x - idealX, PC.c.y - idealY) < iplayer.fear)
	{
		idealX -= Math.sign(PC.c.x - idealX) * iplayer.fear;
		idealY -= Math.sign(PC.c.y - idealY) * iplayer.fear;
	};

	//do not stand on top of teammates
	for (var i = iplayer.team.defensive.length - 1; i >= 0; i--) {
		if (iplayer.team.defensive[i] != iplayer) {
			if (idealY < iplayer.team.defensive[i].circle.y + 5 && idealY > iplayer.team.defensive[i].circle.y - 5) {
				if (idealY < iplayer.team.defensive[i].circle.y) {
					idealY -= 15;
				}
				else {
					idealY += 15;
				}
				
			};
		};
	};

	//move

	if (Math.hypot(iplayer.circle.x - idealX, iplayer.circle.y - idealY) > 10) {
		if (iplayer.hasBall) {
			iplayer.circle.x += (iplayer.topSpeed * 0.8) * Math.sign(idealX - iplayer.circle.x);
			iplayer.circle.y += (iplayer.topSpeed * 0.8) * Math.sign(idealY - iplayer.circle.y);
		} else{
			iplayer.circle.x += iplayer.topSpeed * Math.sign(idealX - iplayer.circle.x);
			iplayer.circle.y += iplayer.topSpeed * Math.sign(idealY - iplayer.circle.y);
		};
	};

	if ((Math.hypot(iplayer.circle.x - ball.x, iplayer.circle.y - ball.y) < 20) && ((performance.now() - ball.lastPickup) >= 1000)) {
		if (ball.owner == null) {
			//take ball
			ball.lastPickup = performance.now();
			ball.owner = iplayer;
			iplayer.hasBall = true;
			iplayer.team.hasBall = true;
		}
		else if (ball.owner.team != iplayer.team) {
			//take ball
			ball.lastPickup = performance.now();
			ball.owner.hasBall = false;
			ball.owner.team.hasBall = false;
			ball.owner = iplayer;
			iplayer.hasBall = true;
			iplayer.team.hasBall = true;
		};
	}
}

function MoveMid(iplayer, nr) {
	var idealX = iplayer.team.midLine;
	var idealY;

	if (iplayer.hasBall) {
		idealX = (iplayer.team.right) ? 0 : field.image.width;
		idealY = field.image.height / 2;
	}
	else {
		if (iplayer.team.mid.length > 1) {
			idealY = iplayer.team.midTop + (((iplayer.team.midBottom - iplayer.team.midTop) / (iplayer.team.mid.length - 1)) * nr - 1);
		}
		else {
			idealY = iplayer.team.midTop + ((iplayer.team.midBottom - iplayer.team.midTop) / 2);
		}
	}

	//Never make a robot that does not fear humans.
	if (iplayer.team == opposingTeam && Math.hypot(PC.c.x - idealX, PC.c.y - idealY) < iplayer.fear)
	{
		idealX -= Math.sign(PC.c.x - idealX) * iplayer.fear;
		idealY -= Math.sign(PC.c.y - idealY) * iplayer.fear;
	};

	//do not stand on top of teammates
	for (var i = iplayer.team.mid.length - 1; i >= 0; i--) {
		if (iplayer.team.mid[i] != iplayer) {
			if (idealY < iplayer.team.mid[i].circle.y + 5 && idealY > iplayer.team.mid[i].circle.y - 5) {
				if (idealY < iplayer.team.mid[i].circle.y) {
					idealY -= 15;
				}
				else {
					idealY += 15;
				}
				
			};
		};
	};

	//move

	if (Math.hypot(iplayer.circle.x - idealX, iplayer.circle.y - idealY) > 10) {
		if (iplayer.hasBall) {
			iplayer.circle.x += (iplayer.topSpeed * 0.8) * Math.sign(idealX - iplayer.circle.x);
			iplayer.circle.y += (iplayer.topSpeed * 0.8 ) * Math.sign(idealY - iplayer.circle.y);
		} else{
			iplayer.circle.x += iplayer.topSpeed * Math.sign(idealX - iplayer.circle.x);
			iplayer.circle.y += iplayer.topSpeed * Math.sign(idealY - iplayer.circle.y);
		};
	};

	if ((Math.hypot(iplayer.circle.x - ball.x, iplayer.circle.y - ball.y) < 20) && ((performance.now() - ball.lastPickup) >= 1000)) {
		if (ball.owner == null) {
			//take ball
			ball.lastPickup = performance.now();
			ball.owner = iplayer;
			iplayer.hasBall = true;
			iplayer.team.hasBall = true;
		}
		else if (ball.owner.team != iplayer.team) {
			//take ball
			ball.lastPickup = performance.now();
			ball.owner.hasBall = false;
			ball.owner.team.hasBall = false;
			ball.owner = iplayer;
			iplayer.hasBall = true;
			iplayer.team.hasBall = true;
		};
	}
}

function MoveOffensive(iplayer, nr) {
	var idealX = iplayer.team.offensiveLine;
	var idealY;

	if (iplayer.hasBall) {
		idealX = (iplayer.team.right) ? 0 : field.image.width;
		idealY = field.image.height / 2;
	}
	else {
		if (iplayer.team.offensive.lenght > 1) {
			idealY = iplayer.team.offensiveTop + (((iplayer.team.offensiveBottom - iplayer.team.offensiveTop) / (iplayer.team.offensive.length - 1)) * nr - 1); 
		}
		else {
			idealY = iplayer.team.offensiveTop + ((iplayer.team.offensiveBottom - iplayer.team.offensiveTop) / 2);
		}
		if (iplayer.team.hasBall) {
			idealX = (idealX / 2) + (ball.x / 2);
			idealY = (idealY / 2) + (ball.y / 2);
		}
		else {
			if(Math.hypot(ball.x - iplayer.circle.x, ball.y - iplayer.circle.y) < 25) {
				idealX = ball.x;
				idealY = ball.y;
			}
		}
	}

	//Never make a robot that does not fear humans.
	if (iplayer.team == opposingTeam && Math.hypot(PC.c.x - idealX, PC.c.y - idealY) < iplayer.fear)
	{
		idealX -= Math.sign(PC.c.x - idealX) * iplayer.fear;
		idealY -= Math.sign(PC.c.y - idealY) * iplayer.fear;
	};

	//do not stand on top of teammates
	for (var i = iplayer.team.offensive.length - 1; i >= 0; i--) {
		if (iplayer.team.offensive[i] != iplayer) {
			if (idealY < iplayer.team.offensive[i].circle.y + 5 && idealY > iplayer.team.offensive[i].circle.y - 5) {
				if (idealY < iplayer.team.offensive[i].circle.y) {
					idealY -= 15;
				}
				else {
					idealY += 15;
				}
				
			};
		};
	};

	//move

	if (Math.hypot(iplayer.circle.x - idealX, iplayer.circle.y - idealY) > 10) {
		if (iplayer.hasBall) {
			iplayer.circle.x += (iplayer.topSpeed * 0.8) * Math.sign(idealX - iplayer.circle.x);
			iplayer.circle.y += (iplayer.topSpeed * 0.8) * Math.sign(idealY - iplayer.circle.y);
		} else{
			iplayer.circle.x += iplayer.topSpeed * Math.sign(idealX - iplayer.circle.x);
			iplayer.circle.y += iplayer.topSpeed * Math.sign(idealY - iplayer.circle.y);
		};
	};

	if ((Math.hypot(iplayer.circle.x - ball.x, iplayer.circle.y - ball.y) < 20) && ((performance.now() - ball.lastPickup) >= pickupTime)) {
		if (ball.owner == null) {
			//take ball
			ball.lastPickup = performance.now();
			ball.owner = iplayer;
			iplayer.hasBall = true;
			iplayer.team.hasBall = true;
		}
		else if (ball.owner.team != iplayer.team) {
			//take ball
			ball.lastPickup = performance.now();
			ball.owner.hasBall = false;
			ball.owner.team.hasBall = false;
			ball.owner = iplayer;
			iplayer.hasBall = true;
			iplayer.team.hasBall = true;
		};
		pickupTime = (Math.random() * 1500) + 1500;
	}
}

function MoveKeeper(iplayer) {
	idealY = ball.y;

	//move
	if (iplayer.circle.y - idealY > 10 || iplayer.circle.y - idealY < 10) {
		iplayer.circle.y += iplayer.topSpeed * Math.sign(idealY - iplayer.circle.y);
	};
}