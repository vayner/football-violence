var canvas;
var stage;
var field;
var fieldContainer;
var ball;
var referee;
var lineReferee1;
var lineReferee2;
var startText;

var suspicionLevel = 0;
var suspicionData;

var PC;


var crowdSound;

var pickupTime = 2500;
var started = false;
var punching = false;

// main loop
function tick(event) {
	if(event.paused == false) {	
		//timer.update(event.delta);
		
		UpdatePlayers();
		referee.update();
		lineReferee1.update();
		lineReferee2.update();
		updateBall();
		
		PC.update();
		if(punching) {
			punching = false;
			PC.punch();
		}

		scoreBoard.update();
		suspicionData.updateSuspicion();
		stage.update();
	} else {
		punching = false;
	}
}

function postLoadingInit() {
	field = new createjs.Bitmap(preloader.getResult("field"));
	fieldContainer = new createjs.Container();

 	fieldContainer.addChild(field);
	stage.addChild(fieldContainer);

	fieldContainer.x = 15;
	fieldContainer.y = 30;

	startText = new createjs.Text("start", "bold 40px Arial", "#000000");
	startText.text = "CLICK TO START";
	startText.x = (field.image.width / 2) - 140;
	startText.y = (field.image.height / 2) + 7;


	stage.addChild(startText);
	stage.update();
}

function createBall() {
	ball = new createjs.Shape();
	ball.graphics.beginFill("#ffffff").setStrokeStyle(1).beginStroke("#000000").drawCircle(0, 0, 5);
	ball.x = (field.image.width/2);
	ball.y = (field.image.height/2);
	fieldContainer.addChild(ball);
	stage.update();

	ball.owner = null;
	ball.lastPickup = performance.now();
}

function updateBall() {
	if (ball.owner != null) {
		ball.x = ball.owner.circle.x;
		ball.y = ball.owner.circle.y;
	};
}

function suspicionInit() {
	this.place = new createjs.Container();
		
	this.place.x = 600;
	this.place.y = 530;
	
	this.suspicion = new createjs.Text("suspicion: ", "20px Arial", "#000000");
	this.suspicion.text = ("Suspicion: " + suspicionLevel);
	
	this.place.addChild(this.suspicion);
	fieldContainer.addChild(this.place);

	this.suspiciousBar = new progressBar(field.image.width, 30, 0, 100, 0, "#FF0000", "#555555");
	stage.addChildAt(this.suspiciousBar.c,0);
	this.suspiciousBar.c.x = 20;
	this.suspiciousBar.c.y = field.image.height + 30;

	stage.update();
	
	this.updateSuspicion = function() {
		this.suspicion.text = ("Suspicion: " + suspicionLevel);
		this.suspiciousBar.update(suspicionLevel);
		if (suspicionLevel >= 100) {
			gameOver();
		}
	}
}

function StartGame() {
	stage.removeChild(startText);
	scoreBoard.init();
	suspicionData = new suspicionInit();

	createBall();
	PC = new PCbuilder(400, 200);
	referee = new createReferee(ball.x,ball.y-100);
	lineReferee1 = new createLineReferee(1);
	lineReferee2 = new createLineReferee(2);
	friendlyTeam = GenerateTeam("#0000FF", false, 4, 4, 2);
	opposingTeam = GenerateTeam("#FF0000", true, 2, 5, 3);
	fieldContainer.setChildIndex(ball, fieldContainer.getNumChildren() -1);

	createjs.Ticker.setPaused(false);
}

function gameOver() {
	//fieldContainer.removeAllChildren();
	createjs.Ticker.setPaused(true);
	
	endGame = new createjs.Text("Game Over", "bold 40px Arial", "#000000");
	endGame.text = "Game Over!";
	endGame.x = (field.image.width / 2) - 103;
	endGame.y = (field.image.height / 2) + 7;
	stage.addChild(endGame);
	
}

// initializer
function init() {
	stage = new createjs.Stage("FootballField");
	stage.mouseEventsEnabled = true;
	stage.mouseChildren = false;
	startPreload();
}
