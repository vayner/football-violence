// input handling logic

var KEYCODE_ENTER = 13;
var KEYCODE_SPACE = 32;
var KEYCODE_UP = 38;
var KEYCODE_LEFT = 37;
var KEYCODE_RIGHT = 39;
var KEYCODE_W = 87;
var KEYCODE_A = 65;
var KEYCODE_S = 83;
var KEYCODE_D = 68;

function registerInputHandlers() {
	document.onkeydown 		= handleKeyDown;
	document.onkeyup 		= handleKeyUp;
	stage.addEventListener("stagemousemove", handleMouseMove);
	stage.addEventListener("stagemouseup", handleMouseUp);
	stage.addEventListener("stagemousedown", handleMouseDown);
}

function handleKeyDown(event) {
	
	switch (event.keyCode) {
		case KEYCODE_S:
			createjs.Sound.setMute((createjs.Sound.getMute() == false));
		break;
	}
}

function handleKeyUp(event) {
}

function handleMouseMove(event) {
}

function handleMouseUp(event) {
}

function handleMouseDown(event) {
	if(started == false) {
		started = true;
		StartGame();
		return;
	}

	punching = true;
}