// JavaScript Document
function PCbuilder (xStart, yStart) {
	this.c = new createjs.Container();
	this.model = new createjs.Shape();
	this.arrow = new createjs.Shape();
	this.facing = new createjs.Shape();

	this.radi = 10;
	this.c.x = xStart;
	this.c.y = yStart;

	this.model.graphics.beginFill("#2020FF").drawCircle(0, 0, this.radi);
	this.facing.graphics.beginFill("#2020FF").moveTo(3,this.radi).lineTo(this.radi + 8,0).lineTo(3,-this.radi).cp();
	
	this.c.addChild(this.model);
	this.c.addChild(this.facing);
	
	fieldContainer.addChild(this.c);

	this.x_acc = 0;
	this.y_acc = 0;

	this.x_vel = 0;
	this.y_vel = 0;

	this.maxAcc = 3;
	this.maxVel = 6;

	this.borderPadding = this.radi;

	this.border_x_min = 0 + this.borderPadding;
	this.border_y_min = 0 + this.borderPadding;

	this.border_x_max = field.image.width - this.borderPadding;
	this.border_y_max = field.image.height - this.borderPadding;
	
	this.maxPunchDist = 30;
	this.damageMultiplier = 1.5;
	this.scoreMultiplier = 10;

	this.update = function () {
		var mousePoint = this.c.globalToLocal(stage.mouseX,stage.mouseY);
		var dx = mousePoint.x;
		var dy = mousePoint.y;
		var distance = Math.hypot(dx, dy);
		var angle = Math.atan2(dy, dx) - Math.PI / 4;
		var acc = Math.pow(Math.log(distance), 2);

		if(acc > this.maxAcc) {
			acc = this.maxAcc;
		}

		this.x_acc = Math.cos(angle) * acc - Math.sin(angle) * acc;
		this.y_acc = Math.sin(angle) * acc + Math.cos(angle) * acc;

		this.x_vel += this.x_acc;
		this.y_vel += this.y_acc;

		var vel = Math.sqrt(this.x_vel * this.x_vel + this.y_vel * this.y_vel);

		if(vel > this.maxVel) {
			var b = this.maxVel / vel;
			this.x_vel *= b;
			this.y_vel *= b;
		}

		if(distance < 10) {
			this.x_vel = this.y_vel = 0;
		} else if(distance < 25) {
			this.y_vel *= 0.5;
			this.x_vel *= 0.5;
		}
		
		this.c.x += this.x_vel;
		this.c.y += this.y_vel;

		if(this.c.x < this.border_x_min) {
			this.c.x = this.border_x_min;
			this.x_acc = 0;
			this.x_vel = 0;
		} else if (this.c.x > this.border_x_max){
			this.c.x = this.border_x_max;
			this.x_acc = 0;
			this.x_vel = 0;
		}

		if(this.c.y < this.border_y_min) {
			this.c.y = this.border_y_min;
			this.y_acc = 0;
			this.y_vel = 0;
		} else if (this.c.y > this.border_y_max){
			this.c.y = this.border_y_max;
			this.y_acc = 0;
			this.y_vel = 0;
		}

		this.facing.rotation = Math.atan2(this.y_vel, this.x_vel) * 180 / Math.PI;

		if(distance < 10) {
			this.facing.rotation = (angle + Math.PI / 4) * 180 / Math.PI;
		}
	};


	this.punch = function () {
		var closest = null;
		var dist = 9001;

		for (var i = opposingTeam.players.length - 1; i >= 0; i--) {
			var current = opposingTeam.players[i];
			var currentDist = Math.hypot(this.c.x - current.circle.x, this.c.y - current.circle.y);

			if(currentDist < dist) {
				dist = currentDist;
				closest = current;
			}
		};

		if(dist > this.maxPunchDist) {
			var pt1 = this.c.localToLocal(0,0,referee.view)
			var pt2 = this.c.localToLocal(0,0,lineReferee1.view)
			var pt3 = this.c.localToLocal(0,0,lineReferee2.view)
			if (referee.view.hitTest(pt1.x, pt1.y) 
				|| lineReferee1.view.hitTest(pt2.x, pt2.y)
			    || lineReferee2.view.hitTest(pt3.x, pt3.y)) {
				suspicionLevel += 1;
				stage.update(event);
			}
			switch (Math.ceil(Math.random() * 3)) {
				case 1:
					createjs.Sound.play("punchMiss1");
				break;
				case 2:
					createjs.Sound.play("punchMiss2");
				break;
				case 3:
					createjs.Sound.play("punchMiss3");
				break;
			}

			score -= 10;
			return;
		} else {
			var pt1 = this.c.localToLocal(0,0,referee.view)
			var pt2 = this.c.localToLocal(0,0,lineReferee1.view)
			var pt3 = this.c.localToLocal(0,0,lineReferee2.view)
			if (referee.view.hitTest(pt1.x, pt1.y) 
				|| lineReferee1.view.hitTest(pt2.x, pt2.y)
			    || lineReferee2.view.hitTest(pt3.x, pt3.y)) {
				suspicionLevel += 5;
				stage.update(event);
			}
			switch (Math.ceil(Math.random() * 3)) {
				case 1:
					createjs.Sound.play("punchHit1");
				break;
				case 2:
					createjs.Sound.play("punchHit2");
				break;
				case 3:
					createjs.Sound.play("punchHit3");
				break;
			}


			var power = this.maxPunchDist - dist;
			var startHealth = closest.health;
			closest.health -= power * this.damageMultiplier;
			closest.health = (closest.health < 0)? 0 : closest.health;
			closest.fear += 10 + (power / 10);
			var delta = startHealth - closest.health;

			score += delta * delta * this.scoreMultiplier / 100;
			//console.log("healt remaining:" + closest.health);
			
			if(closest.health <= 0) {
				//console.log("KO!!");
				fieldContainer.removeChild(closest.circle);
				opposingTeam.players.splice(opposingTeam.players.indexOf(closest),1);

				var d = opposingTeam.defensive.indexOf(closest);
				var m = opposingTeam.mid.indexOf(closest);
				var o = opposingTeam.offensive.indexOf(closest);

				if(d != -1) {
					opposingTeam.defensive.splice(d,1);
				} else if (m != -1) {
					opposingTeam.mid.splice(m,1);
				} else if (o != -1) {
					opposingTeam.offensive.splice(o,1);
				} else {
					opposingTeam.keeper = null;
				}

				score += 200;
				createjs.Sound.play("cheering");

				if(closest == ball.owner) {
					ball.owner = null;	
				}
			}
		}
	};
}