// JavaScript Document
var score = 0;

var scoreBoard = {

	text: null,

	init: function () {
		text = new createjs.Text("Score: " + score, "20px Arial", "#000000");
		text.x = 20;
		text.y = 560;

		stage.addChild(text);
	},
	update: function () {
		text.text = ("Score: " + Math.floor(score));
	}	
}