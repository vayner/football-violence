// Utility functions

Math.sign = function (value) {
	if(value > 0) {
		return 1	
	} else if (value < 0 ) {
		return -1
	} else {
		return 0;
	}
}

Math.degToRad = function (value) {
	return value * (Math.PI / 180);
}

Math.radToDeg = function (value) {
	return value * (180 / Math.PI);
}

Math.hypot = function (x, y) {
	return Math.sqrt( Math.pow(x,2) + Math.pow(y,2) );
}

Math.vector = function (x_pos, y_pos) {
	this.x = x_pos;
	this.y = y_pos;
	this.distanceToPoint = function (vector) {
		return Math.hypot( vector.x - this.x, vector.y - this.y );
	};
	
	this.getRotation = function () {
		return Math.atan2(this.y, this.x);
	}
	
	this.rotate = function (rad) {
		this.x = Math.cos(rad) * this.x - Math.sin(rad) * this.y;
		this.y = Math.sin(rad) * this.x + Math.cos(rad) * this.y;
	};
}

function progressBar(iWidth, iHeight, iMin, iMax, iValue, iColor, iBgColor) {
	this.bar = new createjs.Shape();
	this.bgBar = new createjs.Shape();

	this.min = iMin;
	this.max = iMax;

	this.width = iWidth;
	this.height = iHeight;

	this.color = iColor;
	this.bgColor = iBgColor;
		
	this.bar.graphics.f(this.color).r(0,0,this.width, this.height);
	this.bgBar.graphics.f(this.bgColor).r(0,0,this.width,this.height);

	this.c = new createjs.Container();
	this.c.addChild(this.bgBar);
	this.c.addChild(this.bar);

	this.update = function (new_value) {
		this.value = new_value;
		this.value = (this.value < this.min) ? this.min : this.value;
		this.value = (this.value > this.max) ? this.max : this.value;
		this.bar.scaleX = (this.value - this.min) / (this.max - this.min);
	};

	this.update(iValue);
};
