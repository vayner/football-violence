// JavaScript Document
var preloader;
var progressText;
var loadingBar;
var manifest = [
{src:"./resources/pitch.png", id:"field"},
{src:"./resources/15_Thousand_People-Stephan_Schutze-203556060.mp3", id:"crowd"},
{src:"./resources/Cheering 3-SoundBible.com-1680253418.mp3", id:"cheering"},
{src:"./resources/Air Horn-SoundBible.com-964603082.mp3", id:"horn"},
{src:"./resources/Super Punch MMA-SoundBible.com-1869306362v1.mp3", id:"punchHit1"},
{src:"./resources/Super Punch MMA-SoundBible.com-1869306362v2.mp3", id:"punchHit2"},
{src:"./resources/Super Punch MMA-SoundBible.com-1869306362v3.mp3", id:"punchHit3"},
{src:"./resources/Woosh-Mark_DiAngelo-4778593v1.mp3", id:"punchMiss1"},
{src:"./resources/Woosh-Mark_DiAngelo-4778593v2.mp3", id:"punchMiss2"},
{src:"./resources/Woosh-Mark_DiAngelo-4778593v3.mp3", id:"punchMiss3"}
];

function handleFileLoad(event) {
    console.log("A file has loaded of type: " + event.item.type);
    if(event.item.id == "field"){
        console.log("Field is loaded");
    }
}
 
function loadError(evt) {
    console.log("Error!",evt.text);
}
 
 
function handleFileProgress(event) {
    progressText.text = (preloader.progress*100|0) + " % Loaded";
    loadingBar.update(preloader.progress*100|0);
    stage.update();
}

//triggered when all loading is complete
function loadComplete(event) {

	stage.removeChild(progressText);
	stage.removeChild(loadingBar.c);

	createjs.Ticker.setFPS(30);
	createjs.Ticker.setPaused(true);
	createjs.Ticker.addEventListener("tick", tick);
	
	createjs.Sound.setMute(true);

	registerInputHandlers();
	postLoadingInit();
	
	stage.update();
}

function startPreload() {
	loadingBar = new progressBar(200,20,0,100,0,"#AAAAFF","#787878");
	loadingBar.c.x = 250;
	loadingBar.c.y = 50;
	
	progressText = new createjs.Text("", "20px Arial", "#000000");
	progressText.x = 300 - progressText.getMeasuredWidth() / 2;
	progressText.y = 20;

	stage.addChild(loadingBar.c);
	stage.addChild(progressText);
	stage.update();

	createjs.Sound.alternateExtensions = ["mp3"];

	preloader = new createjs.LoadQueue(true);
	preloader.installPlugin(createjs.Sound);
	preloader.on("fileload", handleFileLoad);
	preloader.on("progress", handleFileProgress);
	preloader.on("complete", loadComplete);
	preloader.on("error", loadError);
	preloader.loadManifest(manifest);
}