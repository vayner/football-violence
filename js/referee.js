function createReferee(startX,startY) {
	this.box = new createjs.Container();
	this.circle = new createjs.Shape();
	
	this.box.x = startX;
	this.box.y = startY;
	
	this.angle = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) * (180/Math.PI);
	this.box.rotation = this.angle - 45;
	
	this.circle.graphics.beginFill("#fff701").setStrokeStyle(1).beginStroke("#000000").drawCircle(0, 0, 10);
	
	this.view = new createjs.Shape();
	this.view.graphics.beginFill("#fff701").setStrokeStyle(1).beginStroke("#000000").moveTo(0, 0).lineTo(100, 300)
	.lineTo(300, 100).lineTo(0,0);
	this.view.alpha = 0.2;
	this.box.addChild(this.view);
	
	this.box.addChild(this.circle);
	fieldContainer.addChild(this.box);
	stage.update();
	
	this.update = function() {
		this.angle = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) * (180/Math.PI);
		this.box.rotation = this.angle - 45;

		this.dist = Math.hypot((this.box.x - ball.x), (this.box.y - ball.y));
		var angle = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) - Math.PI / 4;
		this.maxdist = 150;
		this.mindist = 50;
		
		this.x_acc = 0;
		this.y_acc = 0;

		this.x_vel = 0;
		this.y_vel = 0;

		this.maxAcc = 3;
		this.maxVel = 6;

		this.borderPadding = this.radi;
		this.border_x_min = 0 + this.borderPadding;
		this.border_y_min = 0 + this.borderPadding;
		this.border_x_max = field.image.width - this.borderPadding;
		this.border_y_max = field.image.height - this.borderPadding;
		
		var acc = Math.pow(Math.log(this.dist), 2);
		
		if(acc > this.maxAcc) {
			acc = this.maxAcc;
		}

		this.x_acc = Math.cos(angle) * acc - Math.sin(angle) * acc;
		this.y_acc = Math.sin(angle) * acc + Math.cos(angle) * acc;

		this.x_vel += this.x_acc;
		this.y_vel += this.y_acc;

		var vel = Math.sqrt(this.x_vel * this.x_vel + this.y_vel * this.y_vel);

		if(vel > this.maxVel) {
			var b = this.maxVel / vel;
			this.x_vel *= b;
			this.y_vel *= b;
		}

		if(this.dist < 10) {
			this.x_vel = this.y_vel = 0;
		} else if(this.dist < 25) {
			this.y_vel *= 0.5;
			this.x_vel *= 0.5;
		}
		
		if (this.dist > this.maxdist) {
			this.box.x += this.x_vel;
			this.box.y += this.y_vel;
		}
		if (this.dist < this.mindist) {
			this.box.x -= this.x_vel;
			this.box.y -= this.y_vel;
		}
		
		if(this.box.x < this.border_x_min) {
			this.box.x = this.border_x_min;
			this.x_acc = 0;
			this.x_vel = 0;
		} else if (this.box.x > this.border_x_max){
			this.box.x = this.border_x_max;
			this.x_acc = 0;
			this.x_vel = 0;
		}

		if(this.box.y < this.border_y_min) {
			this.box.y = this.border_y_min;
			this.y_acc = 0;
			this.y_vel = 0;
		} else if (this.box.y > this.border_y_max){
			this.box.y = this.border_y_max;
			this.y_acc = 0;
			this.y_vel = 0;
		}
		
		stage.update();
	};
}

function createLineReferee(nr) {
	this.box = new createjs.Container();
	this.circle = new createjs.Shape();
	
	if (nr < 2) {
		this.box.x = (field.image.width/2);
		this.box.y = 475;
	} else {
		this.box.x = (field.image.width/2);
		this.box.y = 1;
	}
	this.box.rotation = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) * (180/Math.PI);

	this.circle.graphics.beginFill("#fff701").setStrokeStyle(1).beginStroke("#000000").drawCircle(0, 0, 10);
	
	this.view = new createjs.Shape();
	this.view.graphics.beginFill("#fff701").setStrokeStyle(1).beginStroke("#000000").moveTo(0, 0).lineTo(150, 350)
	.lineTo(350, 150).lineTo(0,0);
	this.view.alpha = 0.2;
	this.box.addChild(this.view);

	this.box.addChild(this.circle);
	fieldContainer.addChild(this.box);
	stage.update();
	
	this.update = function() {
		this.angle = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) * (180/Math.PI);
		this.box.rotation = this.angle - 45;
		if (nr < 2) {
			if (ball.x > (field.image.width/2) && ball.x < field.image.width) {
				this.box.x = ball.x
				this.angle = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) * (180/Math.PI);
				this.box.rotation = this.angle - 45;
			}
		} else {
			if (ball.x < (field.image.width/2) && ball.x > 0) {
				this.box.x = ball.x
				this.angle = Math.atan2(ball.y - this.box.y, ball.x - this.box.x) * (180/Math.PI);
				this.box.rotation = this.angle - 45;
			}
		}
		stage.update();
	};
}
